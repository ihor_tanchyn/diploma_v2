from django.db import models


class BaseModel(models.Model):
    """Base model."""
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        """Base model settings."""
        abstract = True