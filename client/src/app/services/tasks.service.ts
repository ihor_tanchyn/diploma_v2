import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { UserService } from "./user.service";
import {Note, TodoList, Task} from "../shared/models";

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  public host:string;
  private _retrieveNoteUrl: string;
  private _createNoteUrl: string;
  private _retrieveTodoListUrl: string;
  private _createTodoListUrl: string;
  private _retrieveTaskUrl: string;
  private _createTaskUrl: string;

  constructor(private http: HttpClient) {
    this.host = environment.host;
    this._retrieveNoteUrl = this.host + '/api/note/';
    this._createNoteUrl = this.host + '/api/note/create/';
    this._retrieveTodoListUrl = this.host + '/api/todo-list/';
    this._createTodoListUrl = this.host + '/api/todo-list/create/';
    this._createTaskUrl = this.host + '/api/task/create/';
    this._retrieveTaskUrl = this.host + '/api/task/';
  }

  public deleteNote(pk: number) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.delete(this._retrieveNoteUrl + pk + '/', {headers})
  }

  public createNote(note: Note) {
    let requestData = {
      title: note.title,
      description: note.description,
      tags: note.tags.join(','),
      workspace: note.workspace
    };
    let headers = UserService.createAuthorizationHeader();
    return this.http.post<Note>(this._createNoteUrl, requestData, {headers});
  }

  public editNote(note: Note) {
    let requestData = {
      title: note.title,
      description: note.description,
      tags: note.tags.join(','),
      workspace: note.workspace
    };
    let headers = UserService.createAuthorizationHeader();
    return this.http.put<Note>(this._retrieveNoteUrl + note.id + '/', requestData, {headers});
  }

  public createTodoList(todoList: TodoList) {
    let requestData = {
      title: todoList.title,
      workspace: todoList.workspace,
      tags: todoList.tags.join(',')
    };

    let headers = UserService.createAuthorizationHeader();
    return this.http.post<TodoList>(this._createTodoListUrl, requestData, {headers})
  }

  public updateTodoList(todoList: TodoList) {
    let requestData = {
      title: todoList.title,
      workspace: todoList.workspace,
      tags: todoList.tags.join(',')
    };
    let headers = UserService.createAuthorizationHeader();
    return this.http.put<TodoList>(this._retrieveTodoListUrl + todoList.id + '/', requestData, {headers})
  }

  public deleteTodoList(pk: number) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.delete(this._retrieveTodoListUrl + pk + '/', {headers})
  }

  public createTask(task: Task) {
    let requestData = {
      title: task.title,
      parent_list: task.parent_list,
      completed: false
    };
    let headers = UserService.createAuthorizationHeader();
    return this.http.post(this._createTaskUrl, requestData, {headers})
  }

  public deleteTask(pk: number) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.delete(this._retrieveTaskUrl + pk + '/', {headers})
  }

  public updateTask(task: Task) {
    let requestData = {
      title: task.title,
      parent_list: task.parent_list,
      completed: task.completed
    };
    let headers = UserService.createAuthorizationHeader();
    return this.http.put<Task>(this._retrieveTaskUrl + task.id + '/', requestData, {headers})
  }
}
