import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "./user.service";


@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public host:string;
  private _createWorkspaceUrl:string;
  private _retrieveWorkspace:string;


  constructor(private http: HttpClient) {
    this.host = environment.host;
    this._createWorkspaceUrl = this.host + '/api/workspace/create/';
    this._retrieveWorkspace = this.host + '/api/workspace/';
  }

  public createWorkspace(newWorkspaceName: string) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.post<any>(this._createWorkspaceUrl, {name: newWorkspaceName}, {headers});
  }

  public deleteWorkspace(pk: number) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.delete<any>(this._retrieveWorkspace + pk + '/', {headers});
  }

}
