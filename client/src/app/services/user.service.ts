import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { User } from "../shared/models";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  public host:string;
  private _loginUrl:string;
  private _logoutUrl:string;
  private _registrationUrl:string;
  private _userInfoUrl:string;
  private _wholeUserInfoUrl:string;

  static createAuthorizationHeader() {
    return new HttpHeaders().set('Authorization', 'Token ' + localStorage.getItem('userToken'))
  }

  constructor(private http: HttpClient) {
    this.host = environment.host;
    this._loginUrl = this.host + '/api/auth/login/';
    this._logoutUrl = this.host + '/api/auth/logout/';
    this._registrationUrl = this.host + '/api/auth/registration/';
    this._userInfoUrl = this.host + '/api/auth/user/';
    this._wholeUserInfoUrl = this.host + '/api/account/full-info/';
  }

  public login(user: User) {
    let requestData = {username: user.email, email: user.email, password: user.password };
    return this.http.post<any>(this._loginUrl, requestData)
      .pipe(map(response => {
        if (response && response.key) {
          localStorage.setItem('userToken', response.key);
        }
        return response;
    }));
  }

  public logout() {
    return this.http.post<any>(this._logoutUrl,  {})
      .pipe(map(response => {
        localStorage.removeItem('userToken');
        localStorage.removeItem('userId');
        return response;
      }));
  }

  public register(user: User) {
    let requestData = {
      email: user.email,
      password:user.password,
      first_name:user.firstName,
      last_name: user.lastName
    };
    return this.http.post<any>(this._registrationUrl,  requestData);
  }

  public currentUser() {
    let headers = UserService.createAuthorizationHeader();
    return this.http.get<any>(this._userInfoUrl, {headers})
      .pipe(map(response => {
        localStorage.setItem('userId', response.pk);
        return response;
      }))
  }

  public allUserInfo(pk: number) {
    let headers = UserService.createAuthorizationHeader();
    return this.http.get<any>(this._wholeUserInfoUrl + pk + '/', {headers})
  }
}
