import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-throbber',
  templateUrl: './throbber.component.html',
  styleUrls: ['./throbber.component.css']
})
export class ThrobberComponent implements OnInit {

  @Input() display: boolean;

  constructor() { }

  ngOnInit() {
  }

}
