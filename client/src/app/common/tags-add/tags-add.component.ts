import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatChipInputEvent} from "@angular/material";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-tags-add',
  templateUrl: './tags-add.component.html',
  styleUrls: ['./tags-add.component.css']
})
export class TagsAddComponent implements OnInit {
  @Input() tags: string[];

  separatorsKeysCodes = [ENTER, COMMA];

  constructor() { }

  ngOnInit() {
  }

  addTag(event: MatChipInputEvent) {
    let input = event.input,
        value = event.value.trim();

    let index = this.tags.indexOf(value);

    if (value && index == -1) {
      this.tags.push(value);
    }

    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: string) {
    let index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }
}
