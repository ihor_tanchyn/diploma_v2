import { Component, OnInit, Input } from '@angular/core';
import { TodoList, Workspace} from "../shared/models";
import {TodolistDialogsComponent} from "../dialogs/todolist-dialogs/todolist-dialogs.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-todolist-card',
  templateUrl: './todolist-card.component.html',
  styleUrls: ['./todolist-card.component.css']
})
export class TodolistCardComponent implements OnInit {

  @Input() todo_list: TodoList;
  @Input() loading: boolean;
  @Input() selectedWorkspace: Workspace;

  constructor(
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
  }

  public openTodoList(todo_list: TodoList) {
    this.dialog.open(TodolistDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, todo_list: todo_list, loading: this.loading, opened: true}})
      .afterClosed().subscribe(
        data => {
        })
  }

  public deleteTodoList(todo_list: TodoList) {
    this.dialog.open(TodolistDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, todo_list: todo_list, loading: this.loading, delete: true}})
      .afterClosed().subscribe(
        data => {
          if (!data) {
            return
          }
          this.selectedWorkspace.todo_lists = this.selectedWorkspace.todo_lists.filter(obj => obj.id !== data.id);
        })
  }

  public renameTodoList(todo_list: TodoList) {
    this.dialog.open(TodolistDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, todo_list: todo_list, loading: this.loading, rename: true}})
      .afterClosed().subscribe(
        data => {
          if (!data) {
            return
          }
          todo_list.title = data.title;
        })
  }

}
