export class User {
  id: number;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

export class Workspace {
  id: number;
  name: string;
  notes: Note[];
  todo_lists: TodoList[];
  created: string;
  updated: string;
  author: number;
}

export class Note {
  id: number;
  title: string;
  description: string;
  workspace: number;
  created: string;
  updated: string;
  author: number;
  tags: string[];
}

export class Task {
  id: number;
  title: string;
  completed: boolean;
  parent_list: number;
  created: string;
  updated: string;
}

export class TodoList {
  id: number;
  title: string;
  tasks: Task[];
  tags: string[];
  author: number;
  workspace: number;
  created: string;
  updated: string;
}
