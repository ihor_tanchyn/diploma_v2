import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import { User } from "../shared/models";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  loading: Boolean;
  user: User;
  signupForm: FormGroup;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.user = new User();
    this.loading = false;
    this.signupForm = this.formBuilder.group({
      firstName: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(128),
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(128),
      ]],
      email: ['', [
        Validators.required,
        Validators.email,
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
      ]],
      passwordRepeat: ['', [
        Validators.required,
        Validators.minLength(8),
      ]]
    }, {validator: SignupComponent.samePasswords});
  }

  static samePasswords(group: FormGroup) {
    let passwordCtrl = group.controls.password,
        passwordRepeatCtrl = group.controls.passwordRepeat;

    if (passwordCtrl.valid && passwordCtrl.value !== passwordRepeatCtrl.value) {
      return passwordRepeatCtrl.setErrors({'differentPasswords': true});
    }
  }

  public onSubmit() {
    if (this.signupForm.invalid) {
      return;
    } else {

      console.log(this.signupForm.controls);

      this.loading = true;
      this.user.email = this.signupForm.controls.email.value;
      this.user.password = this.signupForm.controls.password.value;
      this.user.firstName = this.signupForm.controls.firstName.value;
      this.user.lastName = this.signupForm.controls.lastName.value;

      this.userService.register(this.user).subscribe(
        data => {
          this.toastr.success('Login using your new credentials!', 'Successfully created!');
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 1000);
        },
        error => {
          this.loading = false;

          for (let error_obj of Object.entries(error.error)) {
            this.toastr.error(JSON.stringify(error_obj),'Error!');
          }
        }
      )
    }
  }

  get email() {
    return this.signupForm.get('email');
  }

  get password() {
    return this.signupForm.get('password');
  }

  get passwordRepeat() {
    return this.signupForm.get('passwordRepeat');
  }

  get lastName() {
    return this.signupForm.get('lastName');
  }

  get firstName() {
    return this.signupForm.get('firstName');
  }

}
