import { Component, Input, OnInit } from '@angular/core';
import {Note, Workspace} from "../shared/models";
import {NoteDialogsComponent} from "../dialogs/note-dialogs/note-dialogs.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.css']
})
export class NoteCardComponent implements OnInit {

  @Input() note: Note;
  @Input() loading: boolean;
  @Input() selectedWorkspace: Workspace;

  constructor(
    public NoteDialog: MatDialog,
  ) {}

  ngOnInit() {
  }

  public deleteNote(note:Note) {
    let dialogRef = this.NoteDialog.open(NoteDialogsComponent,
      {data: {delete: true, note: note, loading: this.loading}})
      .afterClosed().subscribe(
        deletedNote => {
          if (!deletedNote) {
            return
          }
          this.selectedWorkspace.notes = this.selectedWorkspace.notes.filter(obj => obj.id !== deletedNote.id);
        }
      )
  }
  public editNote(note: Note) {
    let dialogRef = this.NoteDialog.open(NoteDialogsComponent, {data: {note: note, loading: this.loading}})
      .afterClosed().subscribe(
        data => {
          if (!data) {
            return;
          }
          this.note.tags = data.tags;
          this.note.title = data.title;
          this.note.description = data.description;
        }
      )
  }

}
