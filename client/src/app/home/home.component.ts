import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import {Note, Workspace} from "../shared/models";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTabChangeEvent} from '@angular/material';
import { WorkspaceDialogs } from "../dialogs/create-workspace/create-workspace.component";
import { UtilsService } from "../services/utils.service";
import {NoteDialogsComponent} from "../dialogs/note-dialogs/note-dialogs.component";
import {TodolistDialogsComponent} from "../dialogs/todolist-dialogs/todolist-dialogs.component";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  workspaces: Workspace[];
  selectedWorkspace: Workspace;
  user: any;
  loading: boolean;
  notesTab: boolean;

  constructor(
    public dialog: MatDialog,
    private toastr: ToastrService,
    private utilsService: UtilsService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loading = true;
    let userId = localStorage.getItem('userId');

    if (!userId) {
      this.logout();
      return;
    }

    this.userService.allUserInfo(Number(userId)).subscribe(
      data => {
        this.user = data;
        this.workspaces = this.user.workspaces;
        this.selectedWorkspace = this.workspaces[0];
        this.loading = false;
        this.notesTab = false;
      },
      error => {
        this.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    );
  }

  public toggleTab(tabChangeEvent: MatTabChangeEvent) {
    this.notesTab = tabChangeEvent.index === 1;
  }

  public logout() {
    this.loading = true;
    this.userService.logout().subscribe(
      data => {
        this.router.navigate(['/login']);
        this.loading = false;
      },
      error => {
        this.loading = true;
        console.log(error);
      }
    )
  }

  public openWorkspaceDialog() {
    let dialogRef = this.dialog.open(WorkspaceDialogs, {data: {loading: this.loading}})
      .afterClosed().subscribe(
        workspace => {
          if (!workspace) {
            return;
          }
          this.workspaces.push(workspace);
        }
      )
  }

  public deleteWorkspace(workspace: Workspace) {
    let dialogRef = this.dialog.open(WorkspaceDialogs,
      {data: {delete: true, workspace: workspace, loading: this.loading}})
      .afterClosed().subscribe(
        pk => {
          this.workspaces = this.workspaces.filter(obj => obj.id != pk);

          if (!this.workspaces.length) {
            this.loading = true;
            this.utilsService.createWorkspace('Workspace 1').subscribe(
              data => {
                data.notes = [];
                data.todo_lists = [];
                this.selectedWorkspace = data;
                this.workspaces.push(data);
                this.loading = false;
              },
              error => {
                this.loading = false;
                console.log(error);
              }
            )
          } else {
            this.selectedWorkspace = this.workspaces[0];
          }
        }
      )
  }

  public selectWorkspace(workspace) {
    this.loading = true;
    this.selectedWorkspace = workspace;
    this.loading = false;
  }

  public addTaskOrNote() {
    if (this.notesTab) {
      this.createNote();
    } else {
      this.createTodoList();
    }

  }

  public createNote() {
    let dialogRef = this.dialog.open(NoteDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, loading: this.loading}})
      .afterClosed().subscribe(
      data => {
        if (!data) {
          return;
        }
        this.selectedWorkspace.notes.push(data);
      }
    )
  }

  public createTodoList() {
    let dialogRef = this.dialog.open(TodolistDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, loading: this.loading}})
      .afterClosed().subscribe(
      data => {
        if (!data) {
          return;
        }
        this.dialog.open(TodolistDialogsComponent, {data: {workspaceId: this.selectedWorkspace.id, todo_list: data, loading: this.loading, opened: true}})
          .afterClosed().subscribe(
            data => {

            }
        )
      }
    )
  }

}
