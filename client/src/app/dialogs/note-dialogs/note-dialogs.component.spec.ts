import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteDialogsComponent } from './note-dialogs.component';

describe('NoteDialogsComponent', () => {
  let component: NoteDialogsComponent;
  let fixture: ComponentFixture<NoteDialogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteDialogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteDialogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
