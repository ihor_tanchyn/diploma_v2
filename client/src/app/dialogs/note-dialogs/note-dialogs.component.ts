import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import { UtilsService } from "../../services/utils.service";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Note } from "../../shared/models";
import {TasksService} from "../../services/tasks.service";

@Component({
  selector: 'app-note-dialogs',
  templateUrl: './note-dialogs.component.html',
  styleUrls: ['./note-dialogs.component.css']
})
export class NoteDialogsComponent implements OnInit {

  noteForm: FormGroup;
  tagsList: string[];

  constructor(
    private toastr: ToastrService,
    private taskService: TasksService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<NoteDialogsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    if (this.data && this.data.delete) {
      return;
    }

    let noteCopy = this.data && this.data.note ? JSON.parse(JSON.stringify(this.data.note)) : null;
    let initTitle = noteCopy ? noteCopy.title : '',
        initDescription = noteCopy ? noteCopy.description : '';
    this.tagsList = noteCopy ? noteCopy.tags : [];

    this.noteForm  = this.formBuilder.group({
      title: [initTitle, [
        Validators.required,
        Validators.maxLength(128),
      ]],
      description: [initDescription, [
        Validators.maxLength(5000)
      ]],
    })
  }

  deleteNote(note: Note) {
    this.data.loading = true;
    this.taskService.deleteNote(note.id).subscribe(
      data => {
        this.data.loading = false;
        this.dialogRef.close(note);
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  editNote(note:Note) {
    if (this.noteForm.invalid) {
      return;
    }

    note.title = this.noteForm.controls.title.value;
    note.description = this.noteForm.controls.description.value;
    note.tags = this.tagsList;
    this.data.loading = true;

    this.taskService.editNote(note).subscribe(
      data => {
        this.data.loading = false;
        data.tags = note.tags;
        this.dialogRef.close(data);
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )

  }

  createNote() {
    if (this.noteForm.invalid) {
      return;
    }

    let note = new Note();
    note.title = this.noteForm.controls.title.value;
    note.description = this.noteForm.controls.description.value;
    note.tags = this.tagsList;
    note.workspace = this.data.workspaceId;
    this.data.loading = true;

    this.taskService.createNote(note).subscribe(
      data => {
        this.data.loading = false;
        data.tags = note.tags;
        this.dialogRef.close(data);
      },
      error => {
        this.data.loading = false;
        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  closeDialog() {
    this.dialogRef.close();
  }

  get title() {
    return this.noteForm.get('title');
  }

  get description() {
    return this.noteForm.get('description');
  }

}
