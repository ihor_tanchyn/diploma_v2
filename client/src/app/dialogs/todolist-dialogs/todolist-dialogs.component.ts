import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ToastrService} from "ngx-toastr";
import {WorkspaceDialogs} from "../create-workspace/create-workspace.component";
import {TasksService} from "../../services/tasks.service";
import {Task, TodoList} from "../../shared/models";

@Component({
  selector: 'app-todolist-dialogs',
  templateUrl: './todolist-dialogs.component.html',
  styleUrls: ['./todolist-dialogs.component.css']
})
export class TodolistDialogsComponent implements OnInit {

  todoListName: string;
  todoListTags: string[];
  todoListTitle: string;

  constructor(
    private toastr: ToastrService,
    private taskService: TasksService,
    public dialogRef: MatDialogRef<WorkspaceDialogs>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    if (this.data && this.data.delete) {
      return;
    }

    let copyData = this.data && this.data.todo_list ? JSON.parse(JSON.stringify(this.data.todo_list)) : null;
    this.todoListName = '';
    this.todoListTags = copyData ? copyData.tags : [];
    this.todoListTitle = copyData ? copyData.title : '';
  }

  createTodoList() {
    this.data.loading = true;
    let todoList = new TodoList();
    todoList.title = this.todoListName;
    todoList.workspace = this.data.workspaceId;
    todoList.tags = this.todoListTags;
    this.data.loading = true;

    this.taskService.createTodoList(todoList).subscribe(
      data => {
        this.data.loading = false;
        data.tags = [];
        this.dialogRef.close(data);
      },
      error => {
        this.data.loading = false;
        for (let [error_group, error_message] of Object.entries(error.error)) {
          this.toastr.error(error_message.toString(), 'Error!');
        }
      }
    )
  }

  deleteTodoList(todo_list: TodoList) {
    this.data.loading = true;
    return this.taskService.deleteTodoList(todo_list.id).subscribe(
      data => {
        this.data.loading = false;
        this.dialogRef.close(todo_list);
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  addTask(todo_list:TodoList, newTaskInput: any) {
    let task = new Task();
    task.title = newTaskInput.value;
    task.completed = false;
    task.parent_list = todo_list.id;

    return this.taskService.createTask(task).subscribe(
      data => {
        newTaskInput.value = '';
        this.data.loading = false;
        this.data.todo_list.tasks.push(data);
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  deleteTask(task: Task) {
    this.data.loading = true;
    this.taskService.deleteTask(task.id).subscribe(
      data => {
        this.data.loading = false;
        let index = this.data.todo_list.tasks.indexOf(task);

        if (index >= 0) {
          this.data.todo_list.tasks.splice(index, 1);
        }
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  toggleStatus(task: Task) {
    task.completed = !task.completed;
    this.data.loading = true;

    this.taskService.updateTask(task).subscribe(
      data => {
        this.data.loading = false;
        task.completed = data.completed;
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  updateTodoList(todo_list: TodoList, rename: boolean) {
    this.data.loading = true;

    let todoList = new TodoList();
    todoList.id = todo_list.id;
    todoList.title = this.todoListTitle;
    todoList.workspace = todo_list.workspace;
    todoList.tags = this.todoListTags;

    this.taskService.updateTodoList(todoList).subscribe(
      data => {
        this.data.loading = false;
        todo_list.title = data.title;
        todo_list.tags = this.todoListTags;

        if (rename) {
          this.dialogRef.close(todo_list)
        } else {
          this.toastr.success('Changes were saved!', 'Success!')
        }
      },
      error => {
        this.data.loading = false;

        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
