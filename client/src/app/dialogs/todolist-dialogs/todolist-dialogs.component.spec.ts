import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodolistDialogsComponent } from './todolist-dialogs.component';

describe('TodolistDialogsComponent', () => {
  let component: TodolistDialogsComponent;
  let fixture: ComponentFixture<TodolistDialogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodolistDialogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodolistDialogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
