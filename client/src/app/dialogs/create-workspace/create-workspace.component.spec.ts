import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceDialogs } from './create-workspace.component';

describe('WorkspaceDialogs', () => {
  let component: WorkspaceDialogs;
  let fixture: ComponentFixture<WorkspaceDialogs>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceDialogs ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceDialogs);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
