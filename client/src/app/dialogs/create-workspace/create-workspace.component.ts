import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import { UtilsService } from "../../services/utils.service";
import { Workspace } from "../../shared/models";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-create-workspace',
  templateUrl: './create-workspace.component.html',
  styleUrls: ['./create-workspace.component.css']
})
export class WorkspaceDialogs implements OnInit{

  workspaceName: string;
  workspace: Workspace;

  constructor(
    private toastr: ToastrService,
    private utilsService: UtilsService,
    public dialogRef: MatDialogRef<WorkspaceDialogs>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.workspaceName = '';
  }

  createWorkspace() {
    this.data.loading = true;
    this.utilsService.createWorkspace(this.workspaceName).subscribe(
      data => {
        data.notes = [];
        data.todo_lists = [];
        this.data.loading = false;
        this.dialogRef.close(data);
      },
      error => {
        this.data.loading = false;
        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    );

  }

  deleteWorkspace(pk: number) {
    this.utilsService.deleteWorkspace(pk).subscribe(
      data => {
        this.dialogRef.close(pk);
      },
      error => {
        for (let error_obj of Object.entries(error.error)) {
          this.toastr.error(JSON.stringify(error_obj),'Error!');
        }
      }
    )
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
