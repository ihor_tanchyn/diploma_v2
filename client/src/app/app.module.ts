import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserService } from './services/user.service';
import { HomeComponent } from './home/home.component'
import { AppRoutingModule } from "./app.routing";
import { AppMaterialModule } from "./app.material";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThrobberComponent } from './common/throbber/throbber.component';
import { ToastrModule } from 'ngx-toastr';
import { NoteCardComponent } from './note-card/note-card.component';
import { TodolistCardComponent } from './todolist-card/todolist-card.component';
import { WorkspaceDialogs } from './dialogs/create-workspace/create-workspace.component';
import { NoteDialogsComponent } from './dialogs/note-dialogs/note-dialogs.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TagsAddComponent } from './common/tags-add/tags-add.component';
import { TodolistDialogsComponent } from './dialogs/todolist-dialogs/todolist-dialogs.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    ThrobberComponent,
    NoteCardComponent,
    TodolistCardComponent,
    WorkspaceDialogs,
    NoteDialogsComponent,
    TagsAddComponent,
    TodolistDialogsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
      titleClass: 'custom-toastr-title',
      messageClass: 'custom-toastr-message'
    }),
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    WorkspaceDialogs,
    NoteDialogsComponent,
    TodolistDialogsComponent
  ]
})
export class AppModule { }
