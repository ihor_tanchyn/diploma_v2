import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import { User } from '../shared/models';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  user: User;
  loading: boolean;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = new User();
    this.loading = false;
    this.loginForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
      ]],
    });
  }

  public onSubmit() {
    if (this.loginForm.invalid) {
      return;
    } else {
      this.loading = true;
      this.user.email = this.loginForm.controls.email.value;
      this.user.password = this.loginForm.controls.password.value;

      this.userService.login(this.user).subscribe(
        data => {
          this.userService.currentUser().subscribe(
            data => {
              this.router.navigate(['/']);
              this.loading = false;
            },
            error => {
              this.loading = false;

              for (let error_obj of Object.entries(error.error)) {
                this.toastr.error(JSON.stringify(error_obj),'Error!');
              }
            }
          )
        },
        error => {
          this.loading = false;

          for (let error_obj of Object.entries(error.error)) {
            this.toastr.error(JSON.stringify(error_obj),'Error!');
          }
        }
      )
    }
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

}
