from django.urls import path

from accounts.views import UserRetrieveUpdateAPIView, UserAllInfoAPIView


urlpatterns = [
    path('<int:pk>/', UserRetrieveUpdateAPIView.as_view(), name='account-retrieve-update'),
    path('full-info/<int:pk>/', UserAllInfoAPIView.as_view(), name='current-user-info'),

]
