from rest_framework import serializers

from accounts.models import TodoUser
from utils.serializers import WorkspaceSerializer, WorkspaceExtendedSerializer
from utils.models import Workspace


class TodoUserFullSerializer(serializers.ModelSerializer):

    workspaces = serializers.SerializerMethodField()

    class Meta:
        model = TodoUser
        fields = ('id', 'email', 'username', 'first_name', 'last_name', 'workspaces', 'is_superuser', 'is_active', 'last_login')
        read_only_fields = ('is_superuser', 'last_login', 'is_active', 'username', 'workspaces')

    def get_workspaces(self, obj):
        workspaces = obj.workspaces.all()
        serializer = WorkspaceExtendedSerializer(workspaces, many=True)
        return serializer.data


class TodoUserSerializer(serializers.ModelSerializer):

    workspaces = serializers.SerializerMethodField()

    class Meta:
        model = TodoUser
        fields = ('id', 'email', 'username', 'first_name', 'last_name', 'workspaces', 'is_superuser', 'is_active', 'last_login')
        read_only_fields = ('is_superuser', 'last_login', 'is_active', 'username', 'workspaces')

    def update(self, instance, validated_data):
        updated_instance = super().update(instance, validated_data)

        updated_instance.username = validated_data.get('email')
        updated_instance.save()

        return updated_instance

    def get_workspaces(self, obj):
        workspaces = obj.workspaces.all()
        serializer = WorkspaceSerializer(workspaces, many=True)
        return serializer.data

class CreateUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(label="Password", min_length=8, max_length=64, required=True)

    class Meta:
        model = TodoUser
        fields = ('email', 'password', 'first_name', 'last_name',)
        extra_kwargs = {'email': {'required': True}, 'first_name': {'required': True}, 'last_name': {'required': True}}
        write_only_fields = ('password',)

    def create(self, validated_data):
        validated_data['username'] = validated_data.get('email')

        password = validated_data.pop('password')
        created_user = TodoUser.objects.create(**validated_data)
        created_user.set_password(password)
        created_user.save()

        Workspace.objects.create(author=created_user, name='Workspace 1')

        return created_user
