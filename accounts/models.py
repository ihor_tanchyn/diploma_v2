from django.db import models
from django.contrib.auth.models import AbstractUser

from base.models import BaseModel

# Create your models here.

class TodoUser(AbstractUser):
    """Custom user model"""

    email = models.EmailField('email address', unique=True)

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')

    def __str__(self):
        """TodoUser to string"""
        if self.first_name and self.last_name:
            return "{} {} ({})".format(self.first_name, self.last_name, self.email)
        else:
            return self.email
