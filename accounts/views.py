from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework import generics

from accounts.permissions import IsCurrentUser
from accounts.models import TodoUser
from accounts.serializers import TodoUserSerializer, CreateUserSerializer, TodoUserFullSerializer


class UserRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = TodoUserSerializer
    queryset = TodoUser.objects.all()
    permission_classes = (IsAuthenticated, IsCurrentUser)


class UserCreateAPIView(generics.CreateAPIView):
    serializer_class = CreateUserSerializer
    queryset = TodoUser.objects.all()
    permission_classes = (AllowAny,)


class UserAllInfoAPIView(generics.RetrieveAPIView):
    serializer_class = TodoUserFullSerializer
    permission_classes = (IsAuthenticated, IsCurrentUser)
    queryset = TodoUser.objects.all()
