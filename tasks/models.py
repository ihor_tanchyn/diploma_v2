from django.db import models

from base.models import BaseModel
from accounts.models import TodoUser
from utils.models import Workspace, Tag


class BaseTaskModel(BaseModel):
    title = models.CharField(max_length=256, blank=True, null=True)
    author = models.ForeignKey(TodoUser, related_name='%(class)s', on_delete=models.CASCADE, blank=True, null=True)
    workspace = models.ForeignKey(Workspace, related_name='%(class)s', on_delete=models.CASCADE, blank=True, null=True)
    tags = models.ManyToManyField(Tag, related_name='%(class)s', blank=True)

    @property
    def tags_indexing(self):
        return [tag.title for tag in self.tags.all()]

    @property
    def workspace_indexing(self):
        return self.workspace.name if self.workspace else None

    class Meta:
        abstract = True

    def __str__(self):
        return "{} ({})".format(self.title, self.workspace)


class Note(BaseTaskModel):
    description = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('-created',)


class TodoList(BaseTaskModel):

    class Meta:
        verbose_name = 'todo list'
        verbose_name_plural = 'todo lists'
        ordering = ('-created',)


class Task(BaseModel):
    title = models.CharField(max_length=256, blank=True, null=True)
    parent_list = models.ForeignKey(TodoList, related_name='tasks', on_delete=models.CASCADE, blank=True, null=True)
    completed = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)
