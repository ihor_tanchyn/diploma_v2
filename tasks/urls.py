from django.urls import path

from tasks.views import (NoteRetrieveUpdateDeleteAPIView, NoteCreateAPIView, TodoListCreateAPIView,
                         TodoListRetrieveUpdateDeleteAPIView, TodoListWithTasksListAPIView,
                         TodoListWithTasksRetrieveAPIView, TaskCreateAPIView, TaskRetrieveUpdateDeleteAPIView)


urlpatterns = [
    path('note/<int:pk>/', NoteRetrieveUpdateDeleteAPIView.as_view(), name='note-retrieve-update-delete'),
    path('note/create/', NoteCreateAPIView.as_view(), name='note-create'),
    path('task/<int:pk>/', TaskRetrieveUpdateDeleteAPIView.as_view(), name='task-retrieve-update-delete'),
    path('task/create/', TaskCreateAPIView.as_view(), name='task-create'),
    path('todo-list/<int:pk>/', TodoListRetrieveUpdateDeleteAPIView.as_view(), name='todo-list-create'),
    path('todo-list/create/', TodoListCreateAPIView.as_view(), name='todo-list-create'),
    path('todo-list/all/tasks/', TodoListWithTasksListAPIView.as_view(), name='todo-list-with-tasks-all'),
    path('todo-list/<int:pk>/tasks/', TodoListWithTasksRetrieveAPIView.as_view(), name='todo-list-with-tasks')
]
