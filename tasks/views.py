from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from accounts.permissions import IsOwner
from tasks.models import Task, TodoList, Note
from tasks.serializers import TaskSerializer, TodoListSerializer, NoteSerializer, TodoListWithTasksSerializer, \
    NoteExtendedSerializer, NoteCreateUpdateSerializer, TodoListCreateUpdateSerializer, TodoListExtendedSerializer


class TaskRetrieveUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    permission_classes = (IsAuthenticated,)


class TaskCreateAPIView(generics.CreateAPIView):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    permission_classes = (IsAuthenticated,)


class NoteRetrieveUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    permission_classes = (IsAuthenticated, IsOwner)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return NoteExtendedSerializer
        elif self.request.method == 'PUT':
            return NoteCreateUpdateSerializer
        else:
            return NoteSerializer


class NoteCreateAPIView(generics.CreateAPIView):
    serializer_class = NoteCreateUpdateSerializer
    queryset = Note.objects.all()
    permission_classes = (IsAuthenticated,)


class TodoListRetrieveUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class =  TodoListSerializer
    queryset = TodoList.objects.all()
    permission_classes = (IsAuthenticated, IsOwner)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return TodoListExtendedSerializer
        elif self.request.method == 'PUT':
            return TodoListCreateUpdateSerializer
        else:
            return TodoListSerializer


class TodoListCreateAPIView(generics.CreateAPIView):
    serializer_class = TodoListCreateUpdateSerializer
    queryset = TodoList.objects.all()
    permission_classes = (IsAuthenticated,)


class TodoListWithTasksListAPIView(generics.ListAPIView):
    serializer_class = TodoListWithTasksSerializer
    permission_classes = (IsAuthenticated, IsOwner)

    def get_queryset(self):
        current_user = self.request.user
        return TodoList.objects.filter(author=current_user) if current_user else None


class TodoListWithTasksRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = TodoListWithTasksSerializer
    permission_classes = (IsAuthenticated, IsOwner)

    def get_queryset(self):
        current_user = self.request.user
        return TodoList.objects.filter(author=current_user) if current_user else None