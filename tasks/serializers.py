from rest_framework import serializers

from tasks.models import Note, Task, TodoList
from utils.models import Tag


class NoteExtendedSerializer(serializers.ModelSerializer):
    tags = serializers.ListField(source='tags_indexing', child=serializers.CharField())

    class Meta:
        model = Note
        fields = '__all__'
        read_only_fields = ('created', 'updated', 'author')


class NoteSerializer(serializers.ModelSerializer):
    tags = serializers.ListField(source='tags_indexing', child=serializers.CharField())

    class Meta:
        model = Note
        fields = '__all__'
        extra_kwargs = {'title': {'required': True}, 'workspace': {'required': True}, 'description': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')


class NoteCreateUpdateSerializer(serializers.ModelSerializer):
    tags = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = Note
        fields = '__all__'
        extra_kwargs = {'title': {'required': True}, 'workspace': {'required': True}, 'description': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['author_id'] = user.id
        tags_list = validated_data.pop('tags', '').split(',')
        created_note = Note.objects.create(**validated_data)
        tags_objects = []

        for tag_name in tags_list:
            tag_name = tag_name.strip()

            if not tag_name:
                continue

            tag_obj, _ = Tag.objects.get_or_create(author=user, title=tag_name)
            tags_objects.append(tag_obj)

        created_note.tags.set(tags_objects)
        created_note.save()

        return created_note

    def update(self, instance, validated_data):
        user = self.context['request'].user
        tags_list = validated_data.pop('tags', '').split(',')
        tags_objects = []

        for tag_name in tags_list:
            tag_name = tag_name.strip()

            if not tag_name:
                continue

            tag_obj, _ = Tag.objects.get_or_create(author=user, title=tag_name)
            tags_objects.append(tag_obj)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        instance.tags.set(tags_objects)
        instance.save()

        return instance


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = '__all__'
        extra_kwargs = {
            'title': {'required': True},
            'parent_list': {'required': True},
        }
        read_only_fields = ('created', 'updated')


class TodoListCreateUpdateSerializer(serializers.ModelSerializer):
    tags = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = TodoList
        fields = '__all__'
        extra_kwargs = {'title': {'required': True}, 'workspace': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['author_id'] = user.id
        tags_list = validated_data.pop('tags', '').split(',')
        created_list = TodoList.objects.create(**validated_data)
        tags_objects = []

        for tag_name in tags_list:
            tag_name = tag_name.strip()

            if not tag_name:
                continue

            tag_obj, _ = Tag.objects.get_or_create(author=user, title=tag_name)
            tags_objects.append(tag_obj)

        created_list.tags.set(tags_objects)
        created_list.save()

        return created_list

    def update(self, instance, validated_data):
        user = self.context['request'].user
        tags_list = validated_data.pop('tags', '').split(',')
        tags_objects = []

        for tag_name in tags_list:
            tag_name = tag_name.strip()

            if not tag_name:
                continue

            tag_obj, _ = Tag.objects.get_or_create(author=user, title=tag_name)
            tags_objects.append(tag_obj)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        instance.tags.set(tags_objects)
        instance.save()

        return instance


class TodoListExtendedSerializer(serializers.ModelSerializer):
    tags = serializers.ListField(source='tags_indexing', child=serializers.CharField())

    class Meta:
        model = TodoList
        fields = '__all__'
        read_only_fields = ('created', 'updated', 'author')


class TodoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = TodoList
        fields = '__all__'
        extra_kwargs = {'title': {'required': True}, 'workspace': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')


class TodoListWithTasksSerializer(serializers.ModelSerializer):
    tasks = serializers.SerializerMethodField()
    tags = serializers.ListField(source='tags_indexing', child=serializers.CharField())

    class Meta:
        model = TodoList
        fields = '__all__'
        extra_kwargs = {
            'title': {'required': True},
            'workspace': {'required': True},
        }
        read_only_fields = ('created', 'updated', 'author', 'tasks')

    def get_tasks(self, obj):
        tasks = obj.tasks.all()
        serializer = TaskSerializer(tasks, many=True)
        return serializer.data
