from rest_framework import serializers

from tasks.serializers import TodoListWithTasksSerializer, NoteExtendedSerializer
from utils.models import Tag, Workspace


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Tag
        extra_kwargs = {'title': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['author_id'] = user.id
        created_tag = Tag.objects.create(**validated_data)
        return created_tag


class WorkspaceSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Workspace
        extra_kwargs = {'name': {'required': True}}
        read_only_fields = ('created', 'updated', 'author')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['author_id'] = user.id
        created_workspace = Workspace.objects.create(**validated_data)
        return created_workspace


class WorkspaceExtendedSerializer(serializers.ModelSerializer):
    notes = serializers.SerializerMethodField()
    todo_lists = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = Workspace

    def get_notes(self, obj):
        notes = obj.note.filter(author=obj.author)
        serializer = NoteExtendedSerializer(notes, many=True)
        return serializer.data

    def get_todo_lists(self, obj):
        todo_lists = obj.todolist.filter(author=obj.author)
        serializer = TodoListWithTasksSerializer(todo_lists, many=True)
        return serializer.data
