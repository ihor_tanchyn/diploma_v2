from django.urls import path

from utils.views import (TagCreateAPIView, TagRetrieveDeleteAPIView, TagListAPIView,
                        WorkspaceCreateAPIView, WorkspaceListAPIView, WorkspaceRetrieveDeleteAPIView)

urlpatterns = [
    path('tag/<int:pk>/', TagRetrieveDeleteAPIView.as_view(), name='tag-retrieve-delete'),
    path('tag/create/', TagCreateAPIView.as_view(), name='tag-create'),
    path('tag/list/', TagListAPIView.as_view(), name='tags-list'),
    path('workspace/<int:pk>/', WorkspaceRetrieveDeleteAPIView.as_view(), name='workspace-retrieve-delete'),
    path('workspace/create/', WorkspaceCreateAPIView.as_view(), name='workspace-create'),
    path('workspace/list/', WorkspaceListAPIView.as_view(), name='workspaces-list'),
]
