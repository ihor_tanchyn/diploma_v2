from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from accounts.permissions import IsOwner
from utils.models import Tag, Workspace
from utils.serializers import TagSerializer, WorkspaceSerializer


class TagRetrieveDeleteAPIView(generics.RetrieveDestroyAPIView):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    permission_classes = (IsAuthenticated, IsOwner)


class TagCreateAPIView(generics.CreateAPIView):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    permission_classes = (IsAuthenticated,)


class TagListAPIView(generics.ListCreateAPIView):
    serializer_class = TagSerializer
    permission_classes = (IsAuthenticated, IsOwner)

    def get_queryset(self):
        current_user = self.request.user
        return Tag.objects.filter(author=current_user) if current_user else None


class WorkspaceRetrieveDeleteAPIView(generics.RetrieveDestroyAPIView):
    serializer_class = WorkspaceSerializer
    queryset = Workspace.objects.all()
    permission_classes = (IsAuthenticated, IsOwner)


class WorkspaceCreateAPIView(generics.CreateAPIView):
    serializer_class = WorkspaceSerializer
    queryset = Workspace.objects.all()
    permission_classes = (IsAuthenticated,)


class WorkspaceListAPIView(generics.ListCreateAPIView):
    serializer_class = WorkspaceSerializer
    permission_classes = (IsAuthenticated, IsOwner)

    def get_queryset(self):
        current_user = self.request.user
        return Workspace.objects.filter(author=current_user) if current_user else None