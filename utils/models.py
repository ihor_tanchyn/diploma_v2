from django.db import models

from base.models import BaseModel
from accounts.models import TodoUser


class Workspace(BaseModel):
    name = models.CharField(max_length=128, blank=True, null=True)
    author = models.ForeignKey(TodoUser, related_name='workspaces', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class Tag(BaseModel):
    title = models.CharField(max_length=64, unique=True, blank=True, null=True)
    author = models.ForeignKey(TodoUser, related_name='tags', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title
